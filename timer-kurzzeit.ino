/**
 * Simple program to use a ATTiny45/85 to set a toggle timer
 *
 * Ein einfaches Timer-Programm für den ATTiny45/85 das einen Pin für ein definiertes
 * Zeitintervall schaltet. Nach ablauf des Zeitintervalls wird der Pin umgeschaltet.
 * Beginnend mit LOW wird nach ablauf des Intervalls auf HIGH geschaltet, nach ablauf
 * des Intervalls wieder auf LOW und so weiter.
 *
 * Optional kann man über einen Pin einstellen ob der Ausgabepin für das komplette
 * Intervall auf HIGH gesetzt werden soll oder nur einen HIGH-Impuls ausgiebt und dann
 * wieder auf LOW schaltet (z.B. für bi-stabiele Relais).
 *
 * @license The MIT License
 *
 * Copyright (c) 2019 bastelix
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#if defined(__AVR_ATtinyX5__)

  // pin for the voltage divider. You have to use the analog pin number not the PBx pin constant
  // pin zum lesen des Spannungsteilers. Hier muss die Analog-Pin-Nummer verwendet werden
  const uint8_t ANALOG_PIN = 1;  // PB2;
  // pin to drive the relais or whatever you want to drive
  // pin an dem das relais (oder sonstwas) angeklemmt wird
  const uint8_t OUTPUT_PIN = PB1;
  // this pin configures the switching interval for mono or bi stable relais
  // über diesen pin wird eingestellt ob ein mono- oder bi-stabiles relais geschalten werden soll
  const uint8_t SIGNAL_TIME_PIN = PB0;
  // number of measurements to calculate the arithmetic middle, don't use more than 62 mesaruements
  // a sane number of measurmeents is between 3 and 10
  // anzahl der messung für die Mittelwertsbildung, darf nicht größer als 62 sein!
  // eine vernfünftiger Anzahl an messungen liegt zwischen 3 und 10
  const uint8_t MEASURE_TIMES = 3;
  // tolerance for the voltage divider measurement. Will be added to the defined value before
  // the value is compared to the measurement value
  // toleranz beim auslese des spannungsteilers, wird auf den vergleichswert addiert bevor mit
  // der Rückgabe von analogRead verglichen wird
  const uint8_t TOLERANCE = 2;

#else

  // Wenn der Makro definiert ist werdne über die Serielle Schnittstelle Debug-Ausgaben ausgegen
  // Getestet mit ATMEga328 basiertem Arduino
  #define COMPILE_FOR_ARDUINO

  // Value of the constant resistor. Will be used to calculate the voltage debug output
  // Widerstandswert des festen Widerstands im Spannungsteiler. Wird für die Berechnungen
  // des Spannungswerts der Debug-Ausgabe verwendet
  const uint32_t RESISTOR = 220000L;

  const uint8_t ANALOG_PIN = A2;
  const uint8_t OUTPUT_PIN = 5;
  const uint8_t SIGNAL_TIME_PIN = 6;
  const uint8_t MEASURE_TIMES = 3;
  const uint8_t TOLERANCE = 2;

#endif

  // This is the time in ms which the output pin will set to HIGH if configured to use bi stale relais.
  // Das ist die Zeit für welche der Ausgabepin auf HIGH gesetzt wird,
  // sofern die Ausgabe für bi-stablie Relais configuriert ist.
  const uint16_t SHORT_TIGGER_PULSE = 1000;

// Array mit den analogRead-Werten für das jeweilige Intervall
const uint16_t ANLOG_VALUES_FOR_TIME[] = {
  5,    // 1min  1k
  10,   // 5min  2k2
  15,   // 10min 3k3
  21,   // 15min 4k7
  31,   // 20min 6k8
  44,   // 25min 10k
  65,   // 30min 15k
  93,   // 35min 22k
  133,  // 40min 33k
  180,  // 45min 47k
  242,  // 50min 68k
  320,  // 55min 100k
  512   // 60min 220k
  // fallback 120min
};

// Speichert den Zeitpunkt der letzten Änderung des realayStates in ms
unsigned long startTime = 0;

/**
 * EN
 * With unsingend long timeBase
 * Sketch uses 2190 bytes (26%) of program storage space. Maximum is 8192 bytes.
 * Global variables use 19 bytes (3%) of dynamic memory, leaving 493 bytes for local variables. Maximum is 512 bytes.
 *
 * With uint8_t timeBase and casts
 * Sketch uses 2008 bytes (24%) of program storage space. Maximum is 8192 bytes.
 * Global variables use 17 bytes (3%) of dynamic memory, leaving 495 bytes for local variables. Maximum is 512 bytes.
 *
 * DE
 * Mit uint8_t kann man 2 byte RAM und 182 byte flash sparen.
 * Allerdings wird der Code dann fehleranfälliger weil man manuell casten muss.
 * Da hier noch genügen RAM und flash frei sind lassen wir das mal in der ressourcenhungrigeren Variante!
 *
 */
// timeBase ist die Berechnungsgrundlage der Intervalle in Sekunden
// hier könnte man auch uint8_t verwenden um RAM zu sparen, aber dann muss
// bei jeder Berechnung gecastet werden
unsigned long timeBase = 60L;

// Speichert den aktuellen status des relay pins
uint8_t relayState = 0;

void setup() {
    pinMode(SIGNAL_TIME_PIN, INPUT);
    pinMode(OUTPUT_PIN, OUTPUT);
    digitalWrite(OUTPUT_PIN, LOW);

    /**
     * read the voltage divider for the time base configuration at boot time
     *
     * lesen de Spannungsteilers zum bestimmten der Zeitbasis zur Startzeit
     */
    uint16_t analogValue = readanalogValue();

    /**
     * If there is no time adjusting resitor at boot time the pin is pulled to HIGH
     * then the time base is set to 1 instead of 60 which shortens the switching time
     * to seconds instead of minutes
     *
     * Wenn beim booten kein zeitbestimmender Widerstand vorhanden ist wird der
     * Test-Modus verwendet. Dann entspricht die kleinste Schaltzeit 1 sec statt 1 min
     */
    if (analogValue > 1023 - TOLERANCE) {
        timeBase = 1L;
    }

  #ifdef COMPILE_FOR_ARDUINO
    Serial.begin(9600);
    Serial.println("Timer test code");
    Serial.print("Verwende timeBase=");
    Serial.println(timeBase);
    Serial.println();
  #endif
}

void loop() {
    uint32_t analogValue = readanalogValue();
    unsigned long interval = calcInterval(analogValue);

  #ifdef COMPILE_FOR_ARDUINO
    // This debug code is only compiled if you don not compile for ATTiny*5

    float volt = (analogValue * 5.0) / 1023.0;
    unsigned long now = millis();

    Serial.print("System time ");
    Serial.println(now);

    Serial.print("Current voltage: ");
    Serial.println(volt);
    Serial.print("raw: ");
    Serial.println(analogValue);

    Serial.print("Resistor: ");
    Serial.print((volt * RESISTOR) / (5.0 - volt) / 1000);
    Serial.println("k");

    Serial.print("Current interval ");
    Serial.print(interval / 1000);
    Serial.println("s");

    Serial.print("Last toggle time ");
    Serial.println(startTime);

    Serial.print("Time to toggle ");
    Serial.print((interval - (long) (now - startTime)) / 1000L);
    Serial.println("s");

    Serial.println();

    delay(1000);
  #endif

    // toggle the output pin if the set interval is over
    // schaltet den Ausgabepin wenn das Intervall abgelaufen ist
    if (millis() - startTime >= interval) {
        relayState = !relayState;
        if (digitalRead(SIGNAL_TIME_PIN) == HIGH) {
            digitalWrite(OUTPUT_PIN, relayState);
        } else {
            // use a pulse
            digitalWrite(OUTPUT_PIN, HIGH);
            delay(SHORT_TIGGER_PULSE);
            digitalWrite(OUTPUT_PIN, LOW);
        }
        startTime = millis();
    }
}

/**
 * Read the voltage on ANLOG_PIN for MEASURE_TIMES times, calculate the
 * arithmetic middle and return this value.
 *
 * Liest die Spannung an ANALOG_PIN MEASURE_TIMES mal, berechnet daraus das
 * arithmetische Mittel und gibt dies zurück.
 */
uint32_t readanalogValue() {
    uint32_t analogValue = analogRead(ANALOG_PIN);
    delay(100);

    for (uint8_t i = 0; i < MEASURE_TIMES - 1; ++i) {
      analogValue += analogRead(ANALOG_PIN);
      delay(100);
    }

    // mittelwert bilden
    return analogValue / MEASURE_TIMES;
}

/**
 * Berechnet das Intervall in abhängigkeit der gemessenen Spannung.
 * Wenn die gemessene Spannung < dem Wert in der Mappingtabelle + TOLLERANCE ist wird die Zeit * timeBase in ms Zurückgegeben.
 * Ist der Messwert größer als der größte Wert in der Mapping-Tabelle wird ein Fallback von 120_000 zurückgegeben.
 */
unsigned long calcInterval(const uint16_t analogValue) {
    if (analogValue < ANLOG_VALUES_FOR_TIME[0] + TOLERANCE) {          // 1k
        return timeBase * 1000L;
    } else if (analogValue < ANLOG_VALUES_FOR_TIME[1] + TOLERANCE) {   // 2k2
        return timeBase * 5000L;
    } else if (analogValue < ANLOG_VALUES_FOR_TIME[2] + TOLERANCE) {   // 3k3
        return timeBase * 10000L;
    } else if (analogValue < ANLOG_VALUES_FOR_TIME[3] + TOLERANCE) {   // 4k7
        return timeBase * 15000L;
    } else if (analogValue < ANLOG_VALUES_FOR_TIME[4] + TOLERANCE) {   // 6k8
        return timeBase * 20000L;
    } else if (analogValue < ANLOG_VALUES_FOR_TIME[5] + TOLERANCE) {   // 10k
        return timeBase * 25000L;
    } else if (analogValue < ANLOG_VALUES_FOR_TIME[6] + TOLERANCE) {   // 15k
        return timeBase * 30000L;
    } else if (analogValue < ANLOG_VALUES_FOR_TIME[7] + TOLERANCE) {   // 22k
        return timeBase * 35000L;
    } else if (analogValue < ANLOG_VALUES_FOR_TIME[8] + TOLERANCE) {   // 33k
        return timeBase * 40000L;
    } else if (analogValue < ANLOG_VALUES_FOR_TIME[9] + TOLERANCE) {   // 47k
        return timeBase * 45000L;
    } else if (analogValue < ANLOG_VALUES_FOR_TIME[10] + TOLERANCE) {  // 68k
        return timeBase * 50000L;
    } else if (analogValue < ANLOG_VALUES_FOR_TIME[11] + TOLERANCE) {  // 100k
        return timeBase * 55000L;
    } else if (analogValue < ANLOG_VALUES_FOR_TIME[12] + TOLERANCE) {  // 220k
        return timeBase * 60000L;
    } else {
        return timeBase * 120000L;
    }
}
