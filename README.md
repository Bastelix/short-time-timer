# short-time-timer

This project is documented in english and german.
Dieses Projekt ist in deutsch und englisch dokumentiert.

## EN

A simple timer program for Arduino/ATTiny where the time interval for switching the pin from
high to low and vice versa could be configured by a resistor.

If you want to use a bi-stable relay pull the SWITCH_PIN to GND to get a short HIGH pulse on
the RELAY_PIN. Otherwise pull the SWITCH_PIN to VCC to get a HIGH or LOW signal for the
whole interval. Do not leave the SWITCH_PIN floating!

This project is meant to teach

## DE

Ein einfaches Timer-Programm für Arduino/ATTiny bei dem das Zeitintervall zum umschalten des
Pins vom HIGH auf LOW und umgekehrt über einen Widerstand eingestellt wird.

Wenn ein Bi-Stabieles Relais angesteuert werden soll muss SWITCH_PIN auf GND gezogen werden
um ein kurzes HIGH-Signal am RELAY_PIN zu erhalten. Ansonsten muss der SWITCH_PIN auf VCC
gezogen werden um ein HIGH- oder LOW-Signal für das komplette Intervall zu erhalten.
Der SWITCH_PIN darf nicht floaten!

Dieses Projekt wurde initial erstellt um anderen Menschen

## Circuit for ATTiny85 / Schaltpan für ATTiny85

*Englisch translation for the circuit comments comming soon*

![circuit](circuit.png)
