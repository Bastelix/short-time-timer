# Simple helper script to calcucate the voltage divider
# for different resistor values at R2 within the E12 line
# https://www.elektronik-kompendium.de/sites/slt/0201111.htm

# Total voltage (Vcc)
UGES = 5.0
# Numer of decades for the resistor values
DEKADEN = 2..5

# R1 for the voltage divider
r1 = 220_000 #220k Ohm

# base E12 resistor values
base_e12 = [1, 1.2, 1.5, 1.8, 2.2, 2.7, 3.3, 3.9, 4.7, 5.6, 6.8, 8.2]

# array for R2 values
r2_werte = []

# calculate the voltage U2 for the voltage divider
# returns U2 depending on r1, r2 and UGES
def spannungsteiler_u2(r1, r2)
  ((UGES * r2) / (r1+ r2)).round(4)
end

# calcucate the voltage value returned by analogRead in arduino for the given volate value
def analog_read(volt)
  ((volt * 1023) / UGES).round(0)
end

# iterate over the resistor value decades and fill the array r2_werte with resistor values
DEKADEN.each do |d|
  base_e12.each do |base|
    r2_werte << base * (10 ** d)
  end
end

puts "Bereche Spannungsteiler mit Uges = #{UGES} für E12"
puts ""

# iterate over the R2 resistor values and print the values, U2 and analogRead result
r2_werte.each do |r2|
  u2 = spannungsteiler_u2(r1, r2)
  puts "r1=#{r1} r2=#{r2} U2 = #{u2} #{analog_read(u2)}"
end

